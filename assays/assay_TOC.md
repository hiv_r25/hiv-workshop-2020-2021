 <!-- a normal html comment 
- [CSP2 Schedule](content/csp2_schedule.md)

# Reproducible Analysis
- [Reproducible Analysis Lecture](reproducible/reproducible_research_lecture.md)
- [Git Introduction](reproducible/git_overview.md)
    
# R Introduction

- [R Basics](datascience/)
-->



### Introduction to High-throughput Sequencing (3/18/2021)
  - [High-Throughput Sequencing Theory](rnaseq_bioinformatics/1_hts_background.pdf)
  - [Bioinformatics Overview](rnaseq_bioinformatics/3_bioinformatics_overview.pdf)
  
### Bioinformatics for RNA-seq (3/25/2021)
  - [Bioinformatics Overview (continued)](rnaseq_bioinformatics/3_bioinformatics_overview.pdf)
  - [Quality Score Primer](rnaseq_bioinformatics/notebooks/quality_scores.ipynb)
  - [Bioinformatics Notebook TOC](rnaseq_bioinformatics/notebooks/2021_bioinf_toc.ipynb)
  - Background Material
      - [Jupyter Intro](rnaseq_bioinformatics/notebooks/background/jupyter_intro.ipynb)
      - [Bash Intro](rnaseq_bioinformatics/notebooks/background/unix_intro.ipynb)

### Statistical Analysis for RNA-seq  (4/1/2021)

### Bioinformatics for Flow Cytometry  (4/8/2021)

### Microbiome Analysis (4/15/2021)
  - [Microbiome TOC](microbiome/microbiome_toc.md)

### Bioinformatics for ScRNA-seq (4/22/2021)
  - [scRNA-Seq TOC](sc_rnaseq/sc_rnaseq_toc.md)

