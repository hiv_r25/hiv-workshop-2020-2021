## Microbiome Overview
1. [Microbiome Overview](lecture_slides/microbiome_overview.pdf)

## Microbiome Bioinformatic Analysis
1. [Bioinformatic Analysis Overview](lecture_slides/dada2_pipeline.pdf)
2. [Demultiplex](demultiplex.ipynb)
3. [DADA2: From FASTQs to OTUs](dada2_tutorial_1_6.ipynb)

## Microbiome Statistical Analysis
1. [Statistical Analysis Overview](lecture_slides/statistical_analysis.pdf)
2. [Absolute Abundance Plots](absolute_abundance_plots.ipynb)
3. [Alpha Diversity](alpha_diversity.ipynb)
4. [Relative Abundance](relative_abundance.ipynb)
5. [Beta Diversity & Ordination](ordination.ipynb)

## Appendix
1. [Configuration File](config.R)
2. [Download Data](atacama_download.ipynb)
3. [Download Taxonomic References](download_dada_references.ipynb)
