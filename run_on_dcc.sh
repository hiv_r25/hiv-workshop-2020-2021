DATA_DIR="/work/${USER}/hiv_2021/data"
SCRATCH_DIR="/work/${USER}/hiv_2021/scratch"
mkdir -p $DATA_DIR $SCRATCH_DIR

# srun -A chsi -p chsi --mem=128G --cpus-per-task=40 \

srun --mem=128G --cpus-per-task=40 \
     singularity run \
     --bind ${DATA_DIR}:/data \
     --bind ${SCRATCH_DIR}:${HOME}/work/scratch \
     docker://hivquantworkshop/jupyter-hiv-2020:test \
     jupyter notebook --ip='0.0.0.0' --notebook-dir=${HOME}
#      jupyter notebook --ip='0.0.0.0' --certfile=~/mycert.pem --keyfile ~/mykey.key --notebook-dir=${HOME}
